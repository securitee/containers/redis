ARG REDIS_VERSION

FROM alpine/git:latest AS pull_marblerun
RUN git clone https://github.com/edgelesssys/marblerun.git /marblerun

FROM ghcr.io/edgelesssys/edgelessrt-dev AS build-premain
COPY --from=pull_marblerun /marblerun /premain
WORKDIR /premain/build
RUN cmake -DCMAKE_BUILD_TYPE=RelWithDebInfo ..
RUN make premain-libos

FROM redis:${REDIS_VERSION} AS sign

# add gramine project keyring
ADD https://packages.gramineproject.io/gramine-keyring.gpg /usr/share/keyrings/gramine-keyring.gpg
ENV GRAMINE_KEYRING_SHA256 29c8a028a14f11e3ac3f2ca2669d6ef717656e64d375cd74617319a75db94f13
RUN chmod 0644 /usr/share/keyrings/gramine-keyring.gpg && \
    echo "$GRAMINE_KEYRING_SHA256 /usr/share/keyrings/gramine-keyring.gpg" | sha256sum --check --status

WORKDIR /

# install gramine dependency
RUN apt-get update && apt-get install -y ca-certificates && \
    echo 'deb [arch=amd64 signed-by=/usr/share/keyrings/gramine-keyring.gpg] https://packages.gramineproject.io/ stable main' \
        > /etc/apt/sources.list.d/gramine.list && \
    apt-get update && apt-get install --no-install-recommends -y gcc make binutils gramine && \
    apt-get download gramine libprotobuf-c1

# sign enclave
COPY --from=build-premain /premain/build/premain-libos /usr/local/bin/
COPY Makefile enclave-key.pem redis-server.manifest.template /
RUN make SGX=1

FROM redis:${REDIS_VERSION}

COPY --from=build-premain /premain/build/premain-libos /usr/local/bin/

# copy dependencies and signature
COPY --from=sign /*.deb /redis-server.* /

# install direct dependencies only
RUN dpkg -i --force-depends /*.deb && rm /*.deb

ENTRYPOINT [ "gramine-sgx", "/redis-server"]
