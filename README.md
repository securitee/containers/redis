# Gramine Redis

*Prerequisite:*
* Ensure you have access to a Kubernetes cluster with SGX-enabled nodes and kubectl installed and configured. Follow the marblerun confluence page to setup a kubernetes cluster ready for SGX and marblerun on OVH.
* Ensure you have the [MarbleRun CLI](https://docs.edgeless.systems/marblerun/#/reference/cli) installed.

## Kubernetes deployment walkthrough

We are now installing a Redis server on the cluster.

### Step 0: Generate Admin Certificate and add it to Manifest

* Generate Admin Certificate on a trusted machine

    ```bash
    openssl req -x509 -newkey rsa:4096 -sha256 -days 3650 -nodes -keyout admin_private.key -out admin_certificate.crt
    ```

* Generate the manifest by adding the certificate to the [manifest template](marblerun.manifest.json.template)

    ```bash
    jq --arg cert "$(awk 'NF {sub(/\r/, ""); printf "%s\n",$0;}' admin_certificate.crt)" '.Users.redis.Certificate = $cert' marblerun.manifest.json.template > marblerun.manifest.json
    ```

### Step 1: Installing MarbleRun

First, we are installing MarbleRun on your cluster.

* Install the MarbleRun Coordinator on the Cluster

    ```bash
    marblerun install
    ```

* Wait for the Coordinator to be ready

    ```bash
    marblerun check
    ```

* Port-forward the client API service to localhost

    ```bash
    kubectl -n marblerun port-forward svc/coordinator-client-api 4433:4433 --address localhost >/dev/null &
    export MARBLERUN=localhost:4433
    ```

* Check Coordinator's status, this should return status `2: ready to accept manifest`.

    ```bash
    marblerun status $MARBLERUN
    ```

* Set the manifest

    ```bash
    marblerun manifest set marblerun.manifest.json $MARBLERUN
    ```

### Step 2: Deploying Redis

* Deploy Redis using helm and the default coordinator

    ```bash
    helm install -f ./kubernetes/values.yaml redis ./kubernetes --create-namespace -n redis
    ```

* Alternative: deploy Redis using helm and a coordinator installed in the same namespace

    ```bash
    helm install -f ./kubernetes/values.yaml --set coordinatorInSameNamespace=true redis ./kubernetes --create-namespace -n redis
    ```
* Wait for the Redis server to start, this might take a moment. The output should look like this:

    ```bash
    kubectl logs redis-main-0 -n redis
    ...
    7:M 29 Mar 2021 12:25:40.076 # Server initialized
    7:M 29 Mar 2021 12:25:40.108 * Ready to accept connections
    ```

### Step 3: Using Redis

You can now securely connect to the Redis server using the `redis-cli` and the MarbleRun CA certificate for authentication.

* Make sure you have the latest Redis-CLI with TLS support

* Attestate the coordinator by verifying the manifest and obtaining the Coordinator's CA certificate

    ```bash
    marblerun manifest verify marblerun.manifest.json $MARBLERUN
    marblerun certificate chain $MARBLERUN -o marblerun.crt
    ```

* Retrieve Redis Password from Coordinator

    ```bash
    export REDISPASS=$(curl --silent --cacert marblerun.crt --cert admin_certificate.crt --key admin_private.key https://$MARBLERUN/secrets?s=RedisPassword | jq -r .data.RedisPassword.Private)
    ```

* Connect via the Redis-CLI

    ```bash
    export NODEPORT=$(kubectl -n redis get -o jsonpath="{.spec.ports[0].nodePort}" services redis-main)
    redis-cli -h cluster-domain.example -p $NODEPORT --tls --cacert marblerun.crt -a $REDISPASS
    localhost:6379> set mykey somevalue
    OK
    localhost:6379> get mykey
    "somevalue"
    ```
